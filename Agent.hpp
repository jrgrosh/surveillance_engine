#pragma once
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <memory>
#include "../quadtree/geometry.hpp"

using namespace std;

class Agent{
  private:
    vector<Point<2>>* m_points;
    shared_ptr<vector<Agent>> m_neighbors;
    int m_pointHandle;
    double m_orientation;

    int m_xLowerBound;
    int m_xUpperBound;
    int m_yLowerBound;
    int m_yUpperBound;

    void setPosition(Point<2> pos){
      m_points->at(m_pointHandle) = pos;
    }
  public:
    Agent(vector<Point<2>>* points, int pointHandle, Rectangle<2> worldBox){
      m_points = points;
      m_pointHandle = pointHandle;
      m_orientation = -M_PI/6;
      m_xLowerBound = worldBox.getCenter()[0] - worldBox.getHalfWidth(0);
      m_xUpperBound = worldBox.getCenter()[0] + worldBox.getHalfWidth(0);
      m_yLowerBound = worldBox.getCenter()[1] - worldBox.getHalfWidth(1);
      m_yUpperBound = worldBox.getCenter()[1] + worldBox.getHalfWidth(1);
    }

    int getHandle(){
      return m_pointHandle;
    }

    Point<2> getPosition(){
      return m_points->at(m_pointHandle);
    }

    double getSearchRadius(){
      return 10.0;
    }

    void setNeighbors(shared_ptr<vector<Agent>> neighbors){
      m_neighbors = neighbors;
    }

    void move(){
      Point<2> start = getPosition();
      Point<2> dest;
      dest[0] = start[0] + cos(m_orientation);//+ (double(rand() % 100) - 100)/100.0;
      dest[1] = start[1] + sin(m_orientation);//+ (double(rand() % 100) - 100)/100.0;

      /*
      cout << "Neighbors: " << endl;
      for(int i=0; i< m_neighbors->size(); i++){
        cout << m_neighbors->at(i).getPosition() << endl;
      }
      cout << "***" << endl;
      */
      if(dest[0] < m_xLowerBound || dest[0] > m_xUpperBound){
        m_orientation = M_PI - m_orientation;
        return;
      }

      if(dest[1] < m_yLowerBound || dest[1] > m_yUpperBound){
        m_orientation = - m_orientation;
        return;
      }
      setPosition(dest);


    }


};
