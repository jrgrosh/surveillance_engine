#pragma once
#include "Agent.hpp"
#include "../quadtree/quadtree.hpp"
#include <vector>
#include <memory>

#define LENGTH 5
/*
pass quad tree to agent so they can see which points are near them, then does
lookup in m_agents (maybe just a generic objects-on-map list in future) to dete-
-rmine how to respond (if waldo, follow), if other agent, disperse
*/
class World{
  private:
    QuadTree<2>* m_QT;
    vector<Point<2>>* m_X;
    vector<Agent>* m_agents;
  public:
    World(int num_agents){
      Point<2> p; p[0] = 0.0; p[1] = 0.0;
      Rectangle<2> R(p, LENGTH);

      Point<2> q;
      m_X = new vector<Point<2>>;
      m_agents = new vector<Agent>;
      for(int i = 0; i < num_agents; i++){
        q[0] = (double)(i); q[1] = (double)(i);
        m_X->push_back(q);
        m_agents->push_back(Agent(m_X, i, R));
      }

      m_QT = new QuadTree<2>(R, *m_X); //gross, passed by copy but original remains and is changed via pointer on heap
    }


    shared_ptr<vector<Agent>> getNeighbors(Agent agent){
      auto neighbors = make_shared<vector<Agent>>();
      vector<int> I;
      Rectangle<2> S(agent.getPosition(), agent.getSearchRadius()); //getSearchRadius doesn't exist yet
      m_QT->rangeSearch(I, S);
      for(int i = 0; i<I.size(); i++){
        int index = I[i];
        Agent other = m_agents->at(index);
        //cout << "Rabbits" << endl;
        if(index != agent.getHandle() && euclidDistance(other.getPosition(), agent.getPosition()) < agent.getSearchRadius()){
          neighbors->push_back(other);
        }
      }
      return neighbors;
    }

    void senseStep(){
      for(int i = 0; i <m_agents->size(); i++){
        m_agents->at(i).setNeighbors(getNeighbors(m_agents->at(i)));//m_agents->at(i).move();
      }
    }

    void moveStep(){
      for(int i = 0; i <m_agents->size(); i++){
        m_agents->at(i).move();
      }
      m_QT->rebuild(*m_X);
    }

    void printAgents(){
      for(int i = 0; i < m_agents->size(); i++){
        cout << m_agents->at(i).getPosition() << endl;
      }
      cout << "Do it." << endl;
    }
};
