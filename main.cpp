#include <iostream>
#include <vector>

//#include "../quadtree/quadtree.hpp"
#include "Agent.hpp"
#include "World.hpp"

using namespace std;

int main(){
  World w(1);
  for(int i=0; i<12; i++){
    w.printAgents();
    w.senseStep();
    w.moveStep();
  }
  w.printAgents();
}



/*
int main(){
  cout << "Hello World" << endl;
  Point<2> p;
  p[0] = 0.0;
  p[1] = 0.0;
  int L = 100.0;
  Rectangle<2> R(p, L);


  Point<2> q;
  vector< Point<2> > X;
  for (int i = 0; i < 100; i+=10) {
    for(int j = 0; j < 100; j+= 10){
      cout << i << ", " << j << endl;
      q[0] = double(i); //20.0 * double(rand()) / RAND_MAX - 10.0;
      q[1] = double(j); //20.0 * double(rand()) / RAND_MAX - 10.0;
      X.push_back(q);
    }
  }

  QuadTree<2> QT(R, X);
  cout << "Everything is a file" << endl;

  vector<Agent> agents;
  X.clear();
  for(int i = 0; i < 2; i++){
    q[0] = (double)(i);
    q[1] = (double)(i);
    X.push_back(q);
    agents.push_back(Agent(&X, i));
  }

  cout << "Move 1" << endl;
  for(int i=0; i<agents.size(); i++){
    agents.at(i).move();
  }
  QT.rebuild(X);
  for(int i = 0; i<agents.size(); i++){
    cout << agents.at(i).getPosition() << "\n";
  }
  cout << endl;

  cout << "Move 2" << endl;
  for(int i=0; i<agents.size(); i++){
    agents.at(i).move();
  }
  QT.rebuild(X);
  for(int i = 0; i<agents.size(); i++){
    cout << agents.at(i).getPosition() << "\n";
  }
  cout << endl;

  return 0;
}
*/
